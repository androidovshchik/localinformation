package mrcpp.application.remote;

public interface RequestListener {

    void onSuccess(Object object, String tag);

    void onError(String message);
}
