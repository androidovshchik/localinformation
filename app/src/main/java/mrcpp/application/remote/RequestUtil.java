package mrcpp.application.remote;

import java.io.IOException;

import okhttp3.Request;
import okhttp3.Response;

import timber.log.Timber;

public class RequestUtil {

    public static String getResponse(Response response, boolean showOutput) throws IOException {
        if (!response.isSuccessful()) {
            Timber.e("ERROR: Response is unsuccessful");
            return null;
        }
        String res = response.body().string();
        if (showOutput) {
            Timber.i("OUTPUT: %s", res);
        }
        return res;
    }

    public static Request getRequest(String url, String fromUrl) {
        return new Request.Builder()
                .url(url)
                .tag(fromUrl == null ? url : fromUrl)
                .build();
    }
}
