package mrcpp.application.remote;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import mrcpp.application.Constants;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Response;

import timber.log.Timber;

public class RequestBase implements Callback {

    protected static OkHttpClient client;

    protected RequestListener listener;

    static {
        client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    public RequestBase(RequestListener listener) {
        this.listener = listener;
    }

    public void enqueue(String url) {
        enqueue(url, null);
    }

    public void enqueue(String url, String fromUrl) {
        client.newCall(RequestUtil.getRequest(url, fromUrl)).enqueue(this);
    }

    @Override
    public void onFailure(Call call, IOException e) {
        Timber.e(e.toString());
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        String res;
        switch ((String) call.request().tag()) {
            case Constants.REQUEST_NEWS:
                res = RequestUtil.getResponse(response, false);
                break;
            default:
                res = RequestUtil.getResponse(response, true);
                break;
        }
        if (listener != null) {
            if (res == null || res.isEmpty() || res.equals(Constants.NULL)) {
                return;
            }
            listener.onSuccess(res, (String) call.request().tag());
        }
    }
}
