package mrcpp.application.rss;

import android.support.annotation.NonNull;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class RssReader {

    @SuppressWarnings("all")
    public static List<RssItem> parse(@NonNull String xml, int limit) {
        List<RssItem> items = new ArrayList<>();
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            StringReader reader = new StringReader(xml);
            parser.setInput(reader);
            String title;
            boolean hasNews = false;
            int event = parser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                if (items.size() >= limit + 2) {
                    event = XmlPullParser.END_DOCUMENT;
                    continue;
                }
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        if (name.equals("title")) {
                            hasNews = true;
                        }
                        break;
                    case XmlPullParser.TEXT:
                        if (hasNews) {
                            RssItem item = new RssItem();
                            item.title = parser.getText();
                            items.add(item);
                            hasNews = false;
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        hasNews = false;
                        break;
                }
                event = parser.next();
            }
        } catch (IOException e) {
            Timber.e(e.toString());
        } catch (XmlPullParserException e) {
            Timber.e(e.toString());
        }
        if (items.size() > 0) {
            items.remove(0);
        }
        return items;
    }
}
