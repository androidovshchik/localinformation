package mrcpp.application.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import mrcpp.application.ActivityMain;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Intent activity = new Intent(context, ActivityMain.class);
            activity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(activity);
        }
    }

}