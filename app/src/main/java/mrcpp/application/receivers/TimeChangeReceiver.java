package mrcpp.application.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import mrcpp.application.DataCenter;
import timber.log.Timber;

public class TimeChangeReceiver extends BroadcastReceiver {

    public static final String ACTION = Intent.ACTION_TIME_CHANGED;

    @Override
    public void onReceive(Context context, Intent intent) {
        switch(intent.getAction()) {
            case ACTION:
                Timber.i(intent.getAction());
                if (DataCenter.listener != null) {
                    DataCenter.listener.onTimeChanged(true);
                }
                break;
            default:
                break;
        }
    }
}