package mrcpp.application;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import mrcpp.application.rss.RssItem;
import mrcpp.application.utils.TimeClock;

public class AdapterMain extends RecyclerView.Adapter<AdapterMain.ViewHolder> {

    public List<RssItem> data;

    private final Typeface font;

    public AdapterMain(Context context) {
        this.data = new ArrayList<>();
        font = Typeface.createFromAsset(context.getAssets(), "weather-icons-regular-webfont.ttf");
    }

    public void setData(List<RssItem> data) {
        this.data.clear();
        this.data.addAll(data);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View itemView = null;
        switch (position) {
            case Constants.TYPE_HEADER:
                itemView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.item_header, viewGroup, false);
                break;
            case Constants.TYPE_ITEM:
                itemView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.item_news, viewGroup, false);
                break;
            default:
                break;
        }
        return new ViewHolder(itemView);
    }

    @Override
    @SuppressWarnings("all")
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 0) {
            DataCenter.writeOnViews(holder);
        } else if (position < data.size()) {
            final RssItem item = data.get(position);
            holder.news.setText(item.title);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return Constants.TYPE_HEADER;
        } else {
            return Constants.TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return data.size() + 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.system_time)
        public TimeClock systemTime;
        @Nullable
        @BindView(R.id.web_time)
        public AppCompatTextView webTime;
        @Nullable
        @BindView(R.id.day_of_week)
        public AppCompatTextView dayOfWeek;
        @Nullable
        @BindView(R.id.day_of_month)
        public AppCompatTextView dayOfMonth;
        @Nullable
        @BindView(R.id.degrees)
        public AppCompatTextView degrees;
        @Nullable
        @BindView(R.id.icon)
        public AppCompatTextView icon;
        @Nullable
        @BindView(R.id.humidity)
        public AppCompatTextView humidity;
        @Nullable
        @BindView(R.id.pressure)
        public AppCompatTextView pressure;
        @Nullable
        @BindView(R.id.dollar)
        public AppCompatTextView dollar;
        @Nullable
        @BindView(R.id.euro)
        public AppCompatTextView euro;
        @Nullable
        @BindView(R.id.jams)
        public AppCompatTextView jams;

        @Nullable
        @BindView(R.id.news)
        AppCompatTextView news;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (icon != null) {
                icon.setTypeface(font);
            }
        }
    }
}
