package mrcpp.application;

import android.app.Application;
import android.content.Context;

import timber.log.Timber;

public class AppMain extends Application {

    private static AppMain instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    @SuppressWarnings("unused")
    public synchronized static AppMain getInstance() {
        return instance;
    }

    @SuppressWarnings("unused")
    public static Context getContext() {
        return instance.getApplicationContext();
    }
}