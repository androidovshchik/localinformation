package mrcpp.application.js;

public interface JsListener {

    void onJamInit();

    void onJamValue(int value);
}
