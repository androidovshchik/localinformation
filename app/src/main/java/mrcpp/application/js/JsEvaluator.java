package mrcpp.application.js;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.Locale;

import mrcpp.application.DataCenter;
import mrcpp.application.JsInterface;

import timber.log.Timber;

public class JsEvaluator implements JsListener {

	@SuppressWarnings("unused")
	public final static String FOLDER_ASSETS = "file:///android_asset/";
	@SuppressWarnings("unused")
	public final static String FOLDER_RES_RAW = "file:///android_res/raw/";

	public final String NAMESPACE;

	private WebViewWrapper webViewWrapper;

	@NonNull
	public final Context context;

	@NonNull
	public final Object jsInterface;

	@SuppressWarnings("unused")
	public JsEvaluator(@NonNull Context context, @NonNull Object jsInterface) {
		this(context, jsInterface, "Android");
	}

	@SuppressWarnings("unused")
	public JsEvaluator(@NonNull Context context, @NonNull Object jsInterface, String namespace) {
		Timber.i("JsEvaluator");
		JsInterface.listenerWebView = this;
		this.context = context;
		this.jsInterface = jsInterface;
		this.NAMESPACE = namespace;
	}

	public void evaluate(String folder, String filePath) {
		if (filePath.trim().startsWith("/")) {
			filePath = filePath.substring(1);
		}
		getWebViewWrapper().loadUrl(String.format(Locale.getDefault(), "%s%s",
				folder.trim(), filePath.trim()));
	}

	@Override
	public void onJamInit() {
		if (DataCenter.validLocation()) {
			getWebViewWrapper().postLoadUrl(context, String.format(Locale.getDefault(),
					"init(%s,%s);", parseFloat(DataCenter.lat), parseFloat(DataCenter.lon)));
		}
	}

	private String parseFloat(float var) {
		return String.format(Locale.getDefault(), "%.2f", var).replace(',', '.');
	}

	@Override
	public void onJamValue(int value) {}

	// Destroys the web view in order to free the memory.
	// The web view can not be accessed after is has been destroyed.
	// To check open the page in Chrome: chrome://inspect/#devices
	@SuppressWarnings("unused")
	public void destroy() {
		getWebViewWrapper().destroy(NAMESPACE);
	}

	public WebViewWrapper getWebViewWrapper() {
		if (webViewWrapper == null) {
			webViewWrapper = new WebViewWrapper(context, jsInterface, NAMESPACE);
		}
		return webViewWrapper;
	}
}
