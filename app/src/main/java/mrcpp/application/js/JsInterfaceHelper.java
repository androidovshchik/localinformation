package mrcpp.application.js;

public abstract class JsInterfaceHelper {

	protected String nonNullString(String value) {
		return nonNullString(value, "");
	}

	protected String nonNullString(String value, String defaultValue) {
		return value == null ? defaultValue : value;
	}

	@SuppressWarnings("unused")
	protected boolean tryParseBoolean(String value) {
		return tryParseBoolean(value, false);
	}

	protected boolean tryParseBoolean(String value, boolean defaultValue) {
		try {
			return Boolean.parseBoolean(nonNullString(value));
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	@SuppressWarnings("unused")
	protected long tryParseLong(String value) {
		return tryParseLong(value, 0);
	}

	protected long tryParseLong(String value, long defaultValue) {
		try {
			return Long.parseLong(nonNullString(value));
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	@SuppressWarnings("unused")
	protected int tryParseInt(String value) {
		return tryParseInt(value, 0);
	}

	protected int tryParseInt(String value, int defaultValue) {
		try {
			return Integer.parseInt(nonNullString(value));
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}
}