package mrcpp.application.js;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.util.Locale;

public class WebViewWrapper {

	private WebView webView;

	@SuppressLint({"JavascriptInterface", "SetJavaScriptEnabled"})
	public WebViewWrapper(@NonNull Context context, @NonNull Object jsInterface,
						  @NonNull String namespace) {
		webView = new WebView(context);

		// webView will not draw anything => turn on optimizations
		webView.setWillNotDraw(true);

		final WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setDefaultTextEncodingName("utf-8");
		webView.addJavascriptInterface(jsInterface, namespace);
	}

	public void loadUrl(String url) {
		webView.loadUrl(url);
	}

	public void postLoadUrl(Context context, final String url) {
		Handler handler = new Handler(context.getMainLooper());
		handler.post(new Runnable() {
			@Override
			public void run() {
				loadUrl(String.format(Locale.getDefault(), "%s%s", "javascript:",
						withTryCatch(url)));
			}
		});
	}

	private String withTryCatch(String js){
		return String.format(Locale.getDefault(), "%s%s%s", "javascript:try{", js,
				"}catch(error){Android.onError(error.message);}");
	}

	// Destroys the web view in order to free the memory
	// The web view can not be accessed after is has been destroyed
    // To check open the page in Chrome: chrome://inspect/#devices
	@SuppressWarnings("deprecation")
	public void destroy(@NonNull String namespace) {
		if (webView != null) {
			webView.removeJavascriptInterface(namespace);
			webView.loadUrl("about:blank");
			webView.stopLoading();

			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
				webView.freeMemory();
			}

			webView.clearHistory();
			webView.removeAllViews();
			webView.destroyDrawingCache();
			webView.destroy();

			webView = null;
		}
	}
}
