package mrcpp.application.utils;

public interface TimeListener {

    void onTimeChanged(boolean isSystem);
}
