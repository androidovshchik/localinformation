package mrcpp.application.utils;

public class PostfixUtil {

    public static String[] ENDING_MARK = new String[] {
            /*балл*/"", "а", "ов"
    };

    public static String getEnding(int number, String... endings) {
        int i;
        String ending;
        number = number % 100;
        if (number >= 11 && number <= 19) {
            ending = endings[2];
        }
        else {
            i = number % 10;
            switch (i) {
                case 1:
                    ending = endings[0];
                    break;
                case 2: case 3: case 4:
                    ending = endings[1];
                    break;
                default:
                    ending = endings[2];
            }
        }
        return ending;
    }
}
