package mrcpp.application.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.TextClock;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;

import timber.log.Timber;

public class TimeClock extends TextClock {

    private ArrayList<TextWatcher> listeners;

    public TimeClock(Context context) {
        super(context, null, 0);
    }

    public TimeClock(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public TimeClock(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TimeClock(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @SuppressWarnings("all")
    public Calendar getCalendar() {
        Field field = null;
        try {
            field = TextClock.class.getDeclaredField("mTime");
            field.setAccessible(true);
            Object value = field.get(this);
            if (validFiled(value, Calendar.class)) {
                return (Calendar) value;
            }
        } catch (NoSuchFieldException e) {
            Timber.e(e.toString());
        } catch (IllegalAccessException e) {
            Timber.e(e.toString());
        } finally {
            if (field != null) {
                field.setAccessible(false);
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private boolean validFiled(Object value, Class valueClass) {
        return value != null && valueClass.isAssignableFrom(value.getClass());
    }

    @Override
    public void addTextChangedListener(TextWatcher watcher) {
        if (listeners == null) {
            listeners = new ArrayList<>();
        }
        if (!listeners.isEmpty()) {
            clearTextChangedListeners();
        }
        listeners.add(watcher);
        super.addTextChangedListener(watcher);
    }

    @Override
    public void removeTextChangedListener(TextWatcher watcher) {
        if (listeners != null) {
            int i = listeners.indexOf(watcher);
            if (i >= 0) {
                listeners.remove(i);
            }
        }
        super.removeTextChangedListener(watcher);
    }

    public void clearTextChangedListeners() {
        if (listeners != null) {
            for (TextWatcher watcher : listeners) {
                super.removeTextChangedListener(watcher);
            }
            listeners.clear();
        }
    }
}
