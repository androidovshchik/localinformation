package mrcpp.application;

import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import mrcpp.application.utils.PostfixUtil;
import mrcpp.application.utils.TimeListener;

import timber.log.Timber;

public class DataCenter {

    public static Float lat;

    public static Float lon;

    public static Integer temperature;

    public static Spanned icon;

    public static Integer humidity;

    public static Integer pressure;

    public static Float usd;

    public static Float euro;

    public static Integer jams;

    public static boolean isSystemTimeValid = true;

    public static TimeListener listener;

    public static ScheduledFuture<?> future;

    public static Calendar webCalendar;

    private static TextWatcher watcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (listener != null) {
                listener.onTimeChanged(true);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {}
    };

    public static boolean validLocation() {
        return lat != null && lon != null;
    }

    @SuppressWarnings("all")
    public static void writeOnViews(AdapterMain.ViewHolder holder) {
        Calendar systemCalendar = holder.systemTime.getCalendar();
        isSystemTimeValid = validSystemTime(systemCalendar);

        holder.systemTime.setVisibility(isSystemTimeValid ? View.VISIBLE : View.GONE);
        holder.webTime.setVisibility(isSystemTimeValid ? View.GONE : View.VISIBLE);
        holder.degrees.setVisibility(temperature == null ? View.GONE : View.VISIBLE);
        holder.icon.setVisibility(icon == null ? View.GONE : View.VISIBLE);
        holder.humidity.setVisibility(humidity == null ? View.GONE : View.VISIBLE);
        holder.pressure.setVisibility(pressure == null ? View.GONE : View.VISIBLE);
        holder.dollar.setVisibility(usd == null ? View.GONE : View.VISIBLE);
        holder.euro.setVisibility(euro == null ? View.GONE : View.VISIBLE);
        holder.jams.setVisibility(jams == null ? View.GONE : View.VISIBLE);

        if (isSystemTimeValid) {
            if (future != null) {
                future.cancel(true);
            }
            holder.systemTime.addTextChangedListener(watcher);
            writeDays(holder, systemCalendar);
        } else {
            startTimer();
            holder.systemTime.removeTextChangedListener(watcher);
            holder.webTime.setText(String.format(Locale.getDefault(), "%02d:%02d",
                    webCalendar.get(Calendar.HOUR_OF_DAY), webCalendar.get(Calendar.MINUTE)));
            writeDays(holder, webCalendar);
        }
        if (temperature != null) {
            NumberFormat format = new DecimalFormat("+#;-#");
            holder.degrees.setText(String.format(Locale.getDefault(), "%s",
                    format.format(temperature)));
        }
        if (icon != null) {
            holder.icon.setText(icon);
        }
        if (humidity != null) {
            holder.humidity.setText(String.format(Locale.getDefault(), "Влажность воздуха %d%%",
                    humidity));
        }
        if (pressure != null) {
            holder.pressure.setText(String.format(Locale.getDefault(), "Атмосферное давление %d мм",
                    pressure));
        }
        if (usd != null) {
            holder.dollar.setText(String.format(Locale.getDefault(), "Курс доллара %.2f", usd));
        }
        if (euro != null) {
            holder.euro.setText(String.format(Locale.getDefault(), "Курс евро %.2f", euro));
        }
        if (jams != null) {
            holder.jams.setText(String.format(Locale.getDefault(), "Пробки %d балл%s", jams,
                    PostfixUtil.getEnding(jams, PostfixUtil.ENDING_MARK)));
        }
    }

    private static void startTimer() {
        ScheduledExecutorService timer = Executors.newScheduledThreadPool(1);
        future = timer.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                Timber.i("scheduleWithFixedDelay");
                webCalendar.add(Calendar.MINUTE, 1);
                if (!isSystemTimeValid && listener != null) {
                    listener.onTimeChanged(false);
                }
            }
        }, 1, 1, TimeUnit.MINUTES);
    }

    private static boolean validSystemTime(Calendar systemCalendar) {
        if (webCalendar == null) {
            return true;
        }
        long diffTime = 3 * 60 * 60 * 1000;// 3 hours
        return systemCalendar != null && Math.abs(webCalendar.getTimeInMillis() -
                systemCalendar.getTimeInMillis()) < diffTime;
    }

    @SuppressWarnings("all")
    private static void writeDays(AdapterMain.ViewHolder holder, Calendar calendar) {
        holder.dayOfWeek.setText(calendar.getDisplayName(Calendar.DAY_OF_WEEK,
                Calendar.LONG, Locale.getDefault()));
        int dayInt = calendar.get(Calendar.DAY_OF_MONTH);
        String dayString = calendar.getDisplayName(Calendar.MONTH,
                Calendar.LONG, Locale.getDefault());
        holder.dayOfMonth.setText(String.format(Locale.getDefault(), "%d %s",
                dayInt, dayString));
    }
}
