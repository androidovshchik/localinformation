package mrcpp.application;

import android.webkit.JavascriptInterface;

import mrcpp.application.js.JsInterfaceHelper;
import mrcpp.application.js.JsListener;

import timber.log.Timber;

public class JsInterface extends JsInterfaceHelper {

	public static JsListener listenerWebView;

	public static JsListener listenerCenter;

	public JsInterface() {}

	@JavascriptInterface
	public void onJamsInit() {
		if (listenerWebView != null) {
			listenerWebView.onJamInit();
		}
	}

	@JavascriptInterface
	public void onJamsValue(String value) {
		if (listenerCenter != null) {
			listenerCenter.onJamValue(tryParseInt(value));
		}
	}

	@JavascriptInterface
	public void log(String log) {
		Timber.i(log);
	}

	@JavascriptInterface
	public void onError(String error) {
		Timber.e(nonNullString(error));
	}
}