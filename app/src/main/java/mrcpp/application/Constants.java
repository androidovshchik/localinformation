package mrcpp.application;

import android.Manifest;

public interface Constants {

    String[] ALL_PERMISSIONS = new String[] {
            Manifest.permission.ACCESS_WIFI_STATE
    };

    String NULL = "null";
    String EMPTY = "";

    /* View types */
    int TYPE_HEADER = 1;
    int TYPE_ITEM = 2;

    String WEATHER_API_KEY = "65ca75317334a433072323de9144d2cf";

    String REQUEST_TIME = "http://www.timeapi.org/utc/now";
    String REQUEST_MONEY = "http://api.fixer.io/latest?symbols=USD,RUB";
    String REQUEST_LOCATION = "https://api.mylnikov.org/wifi/main.py/get?v=1.1&bssid=";
    String REQUEST_WEATHER = "http://api.openweathermap.org/data/2.5/weather?lat=%1$f&lon=%2$f&appid=" + WEATHER_API_KEY;
    String REQUEST_NEWS = "https://news.yandex.ru/Vladimir/index.rss";
}
