package mrcpp.application;

import android.Manifest;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import mrcpp.application.js.JsEvaluator;
import mrcpp.application.js.JsListener;
import mrcpp.application.remote.RequestBase;
import mrcpp.application.remote.RequestListener;
import mrcpp.application.rss.RssReader;
import mrcpp.application.utils.ISO8601;
import mrcpp.application.utils.TimeListener;

import timber.log.Timber;

public class ActivityMain extends ActivityBase implements RequestListener, TimeListener, JsListener {

    public static final int REQUEST_PERMISSIONS = 0;

    @BindView(R.id.container)
    RecyclerView recyclerView;

    private AdapterMain adapter;

    private RequestBase request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        DataCenter.listener = this;
        JsInterface.listenerCenter = this;

        adapter = new AdapterMain(getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);

        request = new RequestBase(this);
        makeRequests();

        if (!tryGetWifiInfo()) {
            ActivityCompat.requestPermissions(this, Constants.ALL_PERMISSIONS,
                    REQUEST_PERMISSIONS);
        }

        JsEvaluator jsEvaluator = new JsEvaluator(getApplicationContext(), new JsInterface());
        jsEvaluator.evaluate(JsEvaluator.FOLDER_ASSETS, "jams.html");
    }

    @SuppressWarnings("MissingPermission")
    private boolean tryGetWifiInfo() {
        if (hasPermission(Manifest.permission.ACCESS_WIFI_STATE)) {
            WifiManager wifiMan = (WifiManager) getApplicationContext()
                    .getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiMan.getConnectionInfo();
            if (wifiInfo.getBSSID() == null || wifiInfo.getBSSID().equals("00:00:00:00:00:00")) {
                Timber.w("Wifi FAILED");
                return false;
            } else {
                request.enqueue(String.format(Locale.getDefault(), "%s%s",
                        Constants.REQUEST_LOCATION, wifiInfo.getBSSID()), Constants.REQUEST_LOCATION);
                return true;
            }
        }
        return false;
    }

    private void makeRequests() {
        request.enqueue(Constants.REQUEST_TIME);
        request.enqueue(Constants.REQUEST_NEWS);
        request.enqueue(Constants.REQUEST_MONEY);
    }

    @Override
    public void onTimeChanged(boolean isSystem) {
        Timber.i("onTimeChanged isSystem = %b", isSystem);
        if (!isFinishing() && !recyclerView.isComputingLayout()) {
            notifyHeader();
        }
    }

    private void notifyHeader() {
        if (!adapter.data.isEmpty()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyItemChanged(0);
                }
            });
        }
    }

    @Override
    public void onJamInit() {}

    @Override
    public void onJamValue(int value) {
        DataCenter.jams = value;
        if (!isFinishing() && !recyclerView.isComputingLayout()) {
            notifyHeader();
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onSuccess(Object object, String tag) {
        switch (tag) {
            case Constants.REQUEST_NEWS:
                adapter.setData(RssReader.parse((String) object, 5));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
                break;
            case Constants.REQUEST_MONEY:
                try {
                    JSONObject obj = new JSONObject((String) object);
                    String base = null;
                    if (obj.has("base")) {
                        base = obj.getString("base");
                    }
                    if (obj.has("rates") && base != null && base.equals("EUR")) {
                        JSONObject rates = obj.getJSONObject("rates");
                        if (rates.has("RUB")) {
                            DataCenter.euro = (float) rates.getDouble("RUB");
                        }
                        if (rates.has("USD") && DataCenter.euro != null) {
                            DataCenter.usd = DataCenter.euro / (float) rates.getDouble("USD");
                        }
                    }
                    notifyHeader();
                } catch (JSONException e) {
                    Timber.e(e.toString());
                }
                break;
            case Constants.REQUEST_TIME:
                try {
                    DataCenter.webCalendar = ISO8601.toCalendar((String) object);
                    notifyHeader();
                } catch (ParseException e) {
                    Timber.e(e.toString());
                }
                break;
            case Constants.REQUEST_LOCATION:
                try {
                    JSONObject obj = new JSONObject((String) object);
                    if (obj.getInt("result") == 200) {
                        // success response
                        if (obj.has("data")) {
                            Timber.i("Wifi SUCCESS");
                            JSONObject data = obj.getJSONObject("data");
                            DataCenter.lat = (float) data.getDouble("lat");
                            DataCenter.lon = (float) data.getDouble("lon");
                            Timber.i("lat: %f", DataCenter.lat);
                            Timber.i("lon: %f", DataCenter.lon);
                            request.enqueue(String.format(Locale.getDefault(),
                                    Constants.REQUEST_WEATHER, DataCenter.lat, DataCenter.lon),
                                    Constants.REQUEST_WEATHER);
                            notifyHeader();
                        } else {
                            Timber.i("Wifi FAILED");
                        }
                    } else {
                        Timber.i("Wifi FAILED");
                    }
                } catch (JSONException e) {
                    Timber.i(e.toString());
                }
                break;
            case Constants.REQUEST_WEATHER:
                try {
                    JSONObject obj = new JSONObject((String) object);
                    if (obj.getInt("cod") == 200) {
                        // success response
                        if (obj.has("weather")) {
                            JSONObject weatherObj = obj.getJSONArray("weather").getJSONObject(0);
                            if (weatherObj.has("id")) {
                                try {
                                    JSONArray data = new JSONArray(loadJSONFromAsset());
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject element = data.getJSONObject(i);
                                        int id = element.getInt("id");
                                        if (id == weatherObj.getInt("id")) {
                                            String icon = String.format(Locale.getDefault(),
                                                    "&#x%s;", element.getString("hex"));
                                            DataCenter.icon = Html.fromHtml(icon);
                                        }
                                    }
                                } catch (JSONException e) {
                                    Timber.e(e.toString());
                                }
                            }
                        }
                        if (obj.has("main")) {
                            JSONObject main = obj.getJSONObject("main");
                            if (main.has("temp")) {
                                DataCenter.temperature =
                                        (int) Math.round(main.getDouble("temp") - 273.15);
                            }
                            if (main.has("pressure")) {
                                DataCenter.pressure = (int) Math.round(main.getDouble("pressure"));
                            }
                            if (main.has("humidity")) {
                                DataCenter.humidity = (int) Math.round(main.getDouble("humidity"));
                            }
                        }
                        notifyHeader();
                    }
                } catch (JSONException e) {
                    Timber.e(e.toString());
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onError(String message) {}

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                tryGetWifiInfo();
                break;
            default:
                break;
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("weather-icons.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            if(is.read(buffer) > 0) {
                json = new String(buffer, "UTF-8");
            }
            is.close();
        } catch (IOException e) {
            Timber.e(e.toString());
            return null;
        }
        return json == null ? "[]" : json;
    }
}
